<?php
namespace MC\Forum\ViewHelpers;

use DateTime;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

class TimeDiffViewHelper extends AbstractViewHelper
{

    /**
     * @param null $start
     * @param null $end
     * @return string
     */
    public function render($start = null, $end = null)
    {
        if ($start == null) {
            $start = $this->renderChildren();
        }
        if ($end == null) {
            $end = new DateTime('now');
        }

        if (!($start instanceof DateTime)) {
            $start = new DateTime($start);
        }

        // Plural hinzufügen 'n'
        $interval = $end->diff($start);
        $doPlural = function ($nb, $str) {
            return $nb > 1 ? $str . 'en' : $str;
        };
        $doPluralOnE = function ($nb, $str) {
            return $nb > 1 ? $str . 'n' : $str;
        };

        $format = array();
        if ($interval->y !== 0) {
            $format[] = "%y " . $doPlural($interval->y, "Jahr");
        }
        if ($interval->m !== 0) {
            $format[] = "%m " . $doPlural($interval->m, "Monat");
        }
        if ($interval->d !== 0) {
            $format[] = "%d " . $doPlural($interval->d, "Tag");
        }
        if ($interval->h !== 0) {
            $format[] = "%h " . $doPluralOnE($interval->h, "Stunde");
        }
        if ($interval->i !== 0) {
            $format[] = "%i " . $doPluralOnE($interval->i, "Minute");
        }
        if ($interval->s !== 0) {
            if (!count($format)) {
                $format[] = "%s " . $doPluralOnE($interval->s, "Sekunde");
            }
        } else {
            $format[] = "einigen Sekunden";
        }

        // Die zwei größeren "parts" nutzen
        if (count($format) > 1) {
            $format = array_shift($format) . " und " . array_shift($format);
        } else {
            $format = array_pop($format);
        }

        //berechneten zurückgeben
        return $interval->format($format);
    }
}