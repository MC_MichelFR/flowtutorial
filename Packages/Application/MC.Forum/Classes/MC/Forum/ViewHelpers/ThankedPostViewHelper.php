<?php
namespace MC\Forum\ViewHelpers;

use MC\Forum\Domain\Model\Post;
use MC\Forum\Domain\Model\Thank;
use TYPO3\Flow\Annotations\Inject;
use TYPO3\Fluid\Core\ViewHelper\AbstractConditionViewHelper;


class ThankedPostViewHelper extends AbstractConditionViewHelper
{

    /**
     * @Inject()
     * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
     */
    protected $persistenceManager;

    /**
     * @Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @Inject()
     * @var \MC\Forum\Domain\Repository\ThankRepository
     */
    protected $thankRepository;

    /**
     * @param Post|null $post
     * @return bool
     */
    public function render(Post $post = null)
    {
        if ($post == null) {
            $post = $this->renderChildren();
        }

        $loggedInUser = $this->accountService->getLoggedInUser();

        foreach($post->getThanks() as $thank) {
            /** @var Thank $thank */
            if ($thank->getCreator() == $loggedInUser) {
                return true;
            }
        }
        return false;

    }
}