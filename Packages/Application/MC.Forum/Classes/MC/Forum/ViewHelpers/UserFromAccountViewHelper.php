<?php
namespace MC\Forum\ViewHelpers;

use TYPO3\Flow\Annotations\Inject;
use TYPO3\Flow\Security\Account;
use TYPO3\Fluid\Core\ViewHelper\AbstractViewHelper;

class UserFromAccountViewHelper extends AbstractViewHelper
{

    /**
     * @Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @param Account|null $account
     * @return mixed
     */
    public function render(Account $account = null)
    {
        if ($account == null) {
            $account = $this->renderChildren();
        }
        return $this->accountService->getUserByAccount($account);
    }
}
