<?php
namespace MC\Forum\ViewHelpers;

use TYPO3\Fluid\Core\ViewHelper\AbstractConditionViewHelper;


class MultiIfViewHelper extends AbstractConditionViewHelper
{

    /**
     * @param $condition
     * @param $condition2
     * @param string $operator
     * @return string
     */
    public function render($condition, $condition2, $operator = "and")
    {
        if ($operator == "and") {
            if ($condition && $condition2) {
                return $this->renderThenChild();
            } else {
                return $this->renderElseChild();
            }
        }
        else {
            if ($condition || $condition2) {
                return $this->renderThenChild();
            } else {
                return $this->renderElseChild();
            }
        }
    }
}