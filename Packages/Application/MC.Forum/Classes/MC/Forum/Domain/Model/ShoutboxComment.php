<?php
/**
 * Copyright (c) Michel Ferreira Ribeiro 2016
 */

namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class ShoutboxComment
{

    /**
     * @var string
     */
    protected $content;

    /**
     * @ORM\ManyToOne(inversedBy="shoutboxComments")
     * @var \MC\Forum\Domain\Model\User
     */
    protected $user;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

}
