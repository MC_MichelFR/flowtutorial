<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Subforum
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(inversedBy="subforums")
     * @var Forum
     */
    protected $forum;

    /**
     * @var int
     */
    protected $position;

    /**
     * @ORM\OneToMany(mappedBy="subforum")
     * @ORM\OrderBy({"timestamp" = "DESC"})
     * @var Collection<\MC\Forum\Domain\Model\Topic>
     */
    protected $topics;

    /**
     * Subforum constructor.
     */
    public function __construct()
    {
        $this->topics = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return Forum
     */
    public function getForum()
    {
        return $this->forum;
    }

    /**
     * @param Forum $forum
     */
    public function setForum($forum)
    {
        $this->forum = $forum;
    }

    /**
     * @return Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param Collection $topics
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
    }

    /**
     * @param Topic $topic
     * @return void
     */
    public function addTopic(Topic $topic)
    {
        $this->topics->add($topic);
    }

}
