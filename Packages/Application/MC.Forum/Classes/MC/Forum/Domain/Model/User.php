<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Entity
 */
class User
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $pass;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $lastActivity;

    /**
     * @var string
     */
    protected $role;

    /**
     * @ORM\OneToMany(mappedBy="creator")
     * @var Collection<\MC\Forum\Domain\Model\Message>
     */
    protected $sentMessages;

    /**
     * @ORM\OneToMany(mappedBy="user")
     * @var Collection<\MC\Forum\Domain\Model\ShoutboxComment>
     */
    protected $shoutboxComments;

    /**
     * @ORM\OneToMany(mappedBy="receiver")
     * @var Collection<\MC\Forum\Domain\Model\Message>
     */
    protected $receivedMessages;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $email;

    /**
     * @ORM\OneToOne()
     * @var \TYPO3\Flow\Security\Account
     */
    protected $account;

    /**
     * @ORM\OneToMany(mappedBy="user")
     * @var Collection<\MC\Forum\Domain\Model\Post>
     */
    protected $posts;

    /**
     * @ORM\OneToMany(mappedBy="user")
     * @var Collection<\MC\Forum\Domain\Model\Topic>
     */
    protected $topics;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @var \TYPO3\Flow\Resource\Resource
     * @ORM\OneToOne
     */
    protected $profileImage;

    /**
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * @ORM\OneToMany(mappedBy="receiver")
     * @var Collection<\MC\Forum\Domain\Model\Thank>
     */
    protected $receivedThanks;

    /**
     * @ORM\OneToMany(mappedBy="creator")
     * @var Collection<\MC\Forum\Domain\Model\Thank>
     */
    protected $sentThanks;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->lastActivity = new \DateTime();
        $this->posts = new ArrayCollection();
        $this->topics = new ArrayCollection();
        $this->receivedMessages = new ArrayCollection();
        $this->sendMessages = new ArrayCollection();
        $this->shoutboxComments = new ArrayCollection();
        $this->receivedThanks = new ArrayCollection();
        $this->sentThanks = new ArrayCollection();
    }

    /**
     * @return \TYPO3\Flow\Resource\Resource
     */
    public function getProfileImage()
    {
        return $this->profileImage;
    }

    /**
     * @param \TYPO3\Flow\Resource\Resource $profileImage
     */
    public function setProfileImage($profileImage)
    {
        $this->profileImage = $profileImage;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return \TYPO3\Flow\Security\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \TYPO3\Flow\Security\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Collection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @param Post $post
     * @return void
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @return Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param Collection $topics
     */
    public function setTopics($topics)
    {
        $this->topics = $topics;
    }

    /**
     * @param Topic $topic
     */
    public function addTopic(Topic $topic)
    {
        $this->topics->add($topic);
    }

    /**
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }

    /**
     * @param \DateTime $lastActivity
     */
    public function setLastActivity($lastActivity)
    {
        $this->lastActivity = $lastActivity;
    }

    public function updateLastActivity()
    {
        $this->setLastActivity(new \DateTime('now'));
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return ArrayCollection
     */
    public function getSendMessages()
    {
        return $this->sendMessages;
    }

    /**
     * @param ArrayCollection $sendMessages
     */
    public function setSendMessages($sendMessages)
    {
        $this->sendMessages = $sendMessages;
    }

    /**
     * @return Collection
     */
    public function getSentMessages()
    {
        return $this->sentMessages;
    }

    /**
     * @param Collection $sentMessages
     */
    public function setSentMessages($sentMessages)
    {
        $this->sentMessages = $sentMessages;
    }

    /**
     * @return Collection
     */
    public function getReceivedMessages()
    {
        return $this->receivedMessages;
    }

    /**
     * @param Collection $receivedMessages
     */
    public function setReceivedMessages($receivedMessages)
    {
        $this->receivedMessages = $receivedMessages;
    }

    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return Collection
     */
    public function getShoutboxComments()
    {
        return $this->shoutboxComments;
    }

    /**
     * @param Collection $shoutboxComments
     */
    public function setShoutboxComments($shoutboxComments)
    {
        $this->shoutboxComments = $shoutboxComments;
    }

    /**
     * @param $newShoutboxComment
     */
    public function addShoutboxComment($newShoutboxComment)
    {
        $this->shoutboxComments->add($newShoutboxComment);
    }

    /**
     * @return Collection
     */
    public function getReceivedThanks()
    {
        return $this->receivedThanks;
    }

    /**
     * @param Collection $receivedThanks
     */
    public function setReceivedThanks($receivedThanks)
    {
        $this->receivedThanks = $receivedThanks;
    }

    /**
     * @return Collection
     */
    public function getSentThanks()
    {
        return $this->sentThanks;
    }

    /**
     * @param Collection $sentThanks
     */
    public function setSentThanks($sentThanks)
    {
        $this->sentThanks = $sentThanks;
    }

    /**
     * @param $newThank
     */
    public function addSentThank($newThank){
        $this->sentThanks->add($newThank);
    }

    /**
     * @param $newThank
     */
    public function addReceivedThank($newThank) {
        $this->receivedThanks->add($newThank);
    }

}
