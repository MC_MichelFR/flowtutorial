<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Thank
{
    /**
     * @ORM\ManyToOne(inversedBy="receivedThanks")
     * @var \MC\Forum\Domain\Model\User
     */
    protected $receiver;

    /**
     * @ORM\ManyToOne(inversedBy="sentThanks")
     * @var \MC\Forum\Domain\Model\User
     */
    protected $creator;

    /**
     * @ORM\ManyToOne(inversedBy="thanks")
     * @var \MC\Forum\Domain\Model\Post
     */
    protected $post;

    /**
     * @return mixed
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param mixed $receiver
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param Post $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }
}
