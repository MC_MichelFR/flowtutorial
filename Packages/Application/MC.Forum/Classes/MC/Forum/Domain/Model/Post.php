<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Post
{

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $content;

    /**
     * @ORM\ManyToOne(inversedBy="posts")
     * @var User
     */
    protected $user;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @ORM\ManyToOne(inversedBy="posts")
     * @var Topic
     */
    protected $topic;

    /**
     * @ORM\Column(nullable=true)
     * @var bool
     */
    protected $mainPost;

    /**
     * @ORM\OneToMany(mappedBy="post")
     * @var Collection<\MC\Forum\Domain\Model\Thank>
     */
    protected $thanks;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->thanks = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return boolean
     */
    public function isMainPost()
    {
        return $this->mainPost;
    }

    /**
     * @param boolean $mainPost
     */
    public function setMainPost($mainPost)
    {
        $this->mainPost = $mainPost;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return Topic
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param Topic $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return Collection
     */
    public function getThanks()
    {
        return $this->thanks;
    }

    /**
     * @param Collection $thanks
     */
    public function setThanks($thanks)
    {
        $this->thanks = $thanks;
    }

}
