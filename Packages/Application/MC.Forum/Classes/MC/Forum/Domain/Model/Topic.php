<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Topic
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(inversedBy="topics")
     * @var Subforum
     */
    protected $subforum;

    /**
     * @ORM\ManyToOne(inversedBy="topics")
     * @var User
     */
    protected $user;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @ORM\OneToMany(mappedBy="topic")
     * @ORM\OrderBy({"timestamp" = "ASC"})
     * @var Collection<\MC\Forum\Domain\Model\Post>
     */
    protected $posts;

    /**
     * Topic constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Subforum
     */
    public function getSubforum()
    {
        return $this->subforum;
    }

    /**
     * @param Subforum $subforum
     */
    public function setSubforum($subforum)
    {
        $this->subforum = $subforum;
    }

    /**
     * @return Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Collection $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }


}