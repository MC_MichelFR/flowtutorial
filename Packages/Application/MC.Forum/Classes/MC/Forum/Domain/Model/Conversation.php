<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use MC\Forum\Domain\Repository\ConversationRepository;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Conversation
{

    /**
     * @ORM\OneToMany(mappedBy="conversation")
     * @var Collection<\MC\Forum\Domain\Model\Message>
     */
    protected $messages;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @ORM\ManyToMany(mappedBy="conversations")
     * @var Collection<\MC\Forum\Domain\Model\User>
     */
    protected $users;


    /**
     * Conversation constructor.
     */
    public function __construct()
    {
        $this->messages = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param Collection $messages
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param Message $message
     */
    public function addMessage(Message $message)
    {
        $this->messages->add($message);
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param \DateTime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }


}
