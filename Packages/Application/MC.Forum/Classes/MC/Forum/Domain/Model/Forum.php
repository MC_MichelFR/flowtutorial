<?php
namespace MC\Forum\Domain\Model;

/*
 * This file is part of the MC.Forum package.
 */

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Forum
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $position;

    /**
     * @ORM\OneToMany(mappedBy="forum")
     * @var Collection<\MC\Forum\Domain\Model\Subforum>
     */
    protected $subforums;

    /**
     * Forum constructor.
     */
    public function __construct()
    {
        $this->subforums = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return Collection
     */
    public function getSubforums()
    {
        return $this->subforums;
    }

    /**
     * @param Collection $subforums
     */
    public function setSubforums($subforums)
    {
        $this->subforums = $subforums;
    }

    /**
     * @param Subforum $subforum
     * @return void
     */
    public function addSubforum(Subforum $subforum)
    {
        $this->subforums->add($subforum);
    }

}
