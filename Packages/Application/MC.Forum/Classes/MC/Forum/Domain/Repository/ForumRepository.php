<?php
namespace MC\Forum\Domain\Repository;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class ForumRepository extends Repository
{


    /**
     * @return object
     */
    public function findActive() {
        $query = $this->createQuery();
        return $query->execute()->getFirst();
    }

}
