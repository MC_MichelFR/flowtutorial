<?php
namespace MC\Forum\Domain\Repository;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Subforum;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class TopicRepository extends Repository
{

    // add customized methods here

}