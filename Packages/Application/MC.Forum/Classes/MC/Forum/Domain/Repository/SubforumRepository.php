<?php
namespace MC\Forum\Domain\Repository;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class SubforumRepository extends Repository
{

    // add customized methods here

}
