<?php
namespace MC\Forum\Service;


use MC\Forum\Domain\Model\Message;
use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Security\Account;

/**
 * @Flow\Scope("singleton")
 */
class ConversationService
{
    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\MessageRepository
     */
    protected $messageRepository;

    /**
     * @var PersistenceManagerInterface
     * @Flow\Inject
     */
    protected $persistenceManager;

    /**
     * @var AccountService
     * @Flow\Inject()
     */
    protected $accountService;


    /**
     * @param User $user
     * @return array
     */
    public function getConversations(User $user)
    {
        $conversations = array();
        $conversationMessages = array();

        //get received and sent Messages fom $user
        $messages = $this->getMessages($user);

        foreach ($messages as $message) {
            /** @var Message $message */

            // Reduce duplicates
            if ($message->getReceiver() != $user) {
                $conversationMessages[$this->persistenceManager->getIdentifierByObject($message->getReceiver())] = $message;
            } else {
                $conversationMessages[$this->persistenceManager->getIdentifierByObject($message->getCreator())] = $message;
            }
        }

        // Convert unique userIdentifier Array into an Array of User Objects
        foreach ($conversationMessages as $userIdentifier => $message) {
            $conversations[] = $this->persistenceManager->getObjectByIdentifier($userIdentifier, User::class);
        }

        return $conversations;
    }

    /**
     * @param User $conversationPartner
     * @return array
     */
    public function getConversation(User $conversationPartner)
    {
        $loggedInUser = $this->accountService->getLoggedInUser();
        $conversationMessages = array();

        // dirty....
        $allMessages = $this->messageRepository->findAll();
        foreach ($allMessages as $message) {
            /** @var Message $message */
            if (
                ($message->getCreator() == $loggedInUser && $message->getReceiver() == $conversationPartner) ||
                ($message->getCreator() == $conversationPartner && $message->getReceiver() == $loggedInUser)
            ) {

                // add found message to conversationMessage (and prevent duplicates)
                $conversationMessages[$this->persistenceManager->getIdentifierByObject($message)] = $message;
            }
        }

        usort($conversationMessages, function ($a, $b) {
            return $a->getTimestamp() > $b->getTimestamp();
        });

        return $conversationMessages;
    }



    /**
     * @param User $user
     * @return array
     */
    private function getMessages(User $user)
    {
        //get all messages where $user = creator
        $messagesFrom = $this->messageRepository->findByCreator($user)->toArray();

        //get all messages where $user = receiver
        $messagesTo = $this->messageRepository->findByReceiver($user)->toArray();

        //return both merged in one array
        return array_merge($messagesFrom, $messagesTo);
    }
}