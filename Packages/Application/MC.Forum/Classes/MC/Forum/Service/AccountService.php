<?php
namespace MC\Forum\Service;


use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\PersistenceManagerInterface;
use TYPO3\Flow\Security\Account;

/**
 * @Flow\Scope("singleton")
 */
class AccountService
{

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @var PersistenceManagerInterface
     * @Flow\Inject
     */
    protected $persistenceManager;

    /**
     * @var \TYPO3\Flow\Security\AccountRepository
     * @Flow\Inject()
     */
    protected $accountRepository;


    /**
     * @return User
     */
    public function getLoggedInUser()
    {
        $user = $this->userRepository->findOneByAccount($this->securityContext->getAccount());
        return $user;
    }

    /**
     * @param User $user
     */
    public function updateUser(User $user)
    {
        $this->userRepository->update($user);
        $this->persistenceManager->persistAll();
    }

    /**
     * @return Account
     */
    public function getLoggedInAccount()
    {
        return $this->securityContext->getAccount();
    }

    /**
     * @param Account $account
     */
    public function removeAccount(Account $account)
    {
        $this->accountRepository->remove($account);
        $this->persistenceManager->persistAll();
    }

    /**
     * @param Account $account
     * @return mixed
     */
    public function getUserByAccount(Account $account)
    {
        return $this->userRepository->findOneByAccount($account);
    }

}
