<?php
namespace MC\Forum\AOP;

use MC\Forum\Service\AccountService;
use TYPO3\Flow\Annotations as Flow;

/**
 * @Flow\Aspect
 */
class LastActivityAspect
{

    /**
     * @var AccountService
     * @Flow\Inject
     */
    protected $accountService;

    /**
     * This Aspect updates the last activity of the logged in user
     *
     * @Flow\Before("method(MC\Forum\Controller\.*Controller->.*Action())")
     */
    public function updateLastActivity()
    {
        error_log("Aspect: LastActivity triggered!");

        $user = $this->accountService->getLoggedInUser();

        if ($user != "") {
            $user->updateLastActivity();
            $this->accountService->updateUser($user);
        }
    }
}