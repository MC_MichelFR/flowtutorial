<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\ActionRequest;
use TYPO3\Flow\Security\Authentication\Controller\AbstractAuthenticationController;

class LoginController extends AbstractAuthenticationController
{
    /**
     * @var \MC\Forum\Domain\Repository\UserRepository
     * @Flow\Inject
     */
    protected $userRepository;

    /**
     * @var \TYPO3\Flow\Security\Authentication\AuthenticationManagerInterface
     * @Flow\Inject
     */
    protected $authenticationManager;

    /**
     * @var \TYPO3\Flow\Security\AccountRepository
     * @Flow\Inject
     */
    protected $accountRepository;

    /**
     * @var \TYPO3\Flow\Security\AccountFactory
     * @Flow\Inject
     */
    protected $accountFactory;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * index action, does only display the form
     */
    public function indexAction()
    {
        $currentAccount = $this->securityContext->getAccount();
        $this->view->assign('account', $currentAccount);
    }

    /**
     * @return void
     */
    public function registerAction()
    {
        // do nothing more than display the register form
    }

    /**
     * @param ActionRequest|NULL $originalRequest
     * @return void
     */
    public function onAuthenticationSuccess(ActionRequest $originalRequest = NULL) {
        $this->redirect("index", "Forum");
    }

    /**
     * @param $newUser
     */
    public function createAction(User $newUser)
    {
        error_log("User Created!");

        $roles = array('MC.Forum:User');

        $authenticationProviderName = 'DefaultProvider';
        $account = $this->accountFactory->createAccountWithPassword($newUser->getName(), $newUser->getPass(), $roles, $authenticationProviderName);
        $this->accountRepository->add($account);

        $newUser->setAccount($account);
        $newUser->setCreationDate($account->getCreationDate());
        $newUser->setRole("User");
        $newUser->setPass($account->getCredentialsSource());
        $newUser->setLastActivity(new \DateTime('now'));
        $this->userRepository->add($newUser);

        // add a message and redirect to the login form
        $this->addFlashMessage('Account created. Please login.');

        // redirect to the login form
        $this->redirect('index', 'Login');
    }

    /**
     * @return void
     */
    public function logoutAction()
    {
        $this->authenticationManager->logout();
        $this->addFlashMessage('Successfully logged out.');
        $this->redirect('index', 'Login');
    }

}
