<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Conversation;
use MC\Forum\Domain\Model\Message;
use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;

class MessageController extends \MC\Forum\Controller\AbstractBaseController
{
    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\MessageRepository
     */
    protected $messageRepository;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\ConversationRepository
     */
    protected $conversationRepository;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @return void
     */
    public function indexAction()
    {
        $currentAccount = $this->accountService->getLoggedInAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
            $this->view->assign('user', $this->accountService->getLoggedInUser());
        } else {
            $this->redirect('index', 'login', null, null);
        }
    }

    /**
     * @param Message $message
     */
    public function ShowAction(Message $message)
    {
        $currentAccount = $this->accountService->getLoggedInAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
            $this->view->assign('user', $this->accountService->getLoggedInUser());
        } else {
            $this->redirect('index', 'login', null, null);
        }
        $this->view->assign('message', $message);
    }

    /**
     * @param Message $newMessage
     * @param User $receiver
     */
    public function createNewMessageAction(Message $newMessage, User $receiver)
    {
            $newMessage->setTimestamp(new \DateTime('now'));
            $newMessage->setReceiver($receiver);
            $newMessage->setCreator($this->accountService->getLoggedInUser());

            if($newMessage->getTitle() == null) {
                $newMessage->setTitle("");
            }

            $this->messageRepository->add($newMessage);
            $this->redirect('index', 'conversation', null, array('conversationPartner' => $receiver));
    }

    /**
     * @param Message $message
     */
    public function removeMessage(Message $message)
    {
        $this->messageRepository->remove($message);
        $this->persistenceManager->persistAll();
        $this->redirect('index');
    }
}