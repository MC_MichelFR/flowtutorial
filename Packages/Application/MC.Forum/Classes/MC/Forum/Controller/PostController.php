<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Post;
use MC\Forum\Domain\Model\Topic;
use TYPO3\Flow\Annotations as Flow;

class PostController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\PostRepository
     */
    protected $postRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\TopicRepository
     */
    protected $topicRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @param Post $post
     */
    public function delPostAction(Post $post)
    {
        $topic = $post->getTopic();

        if ($post->isMainPost())
        {
            foreach ($topic->getPosts() as $dpost) {
                $this->postRepository->remove($dpost);
                $this->persistenceManager->persistAll();
            }

            $this->topicRepository->remove($topic);
            $this->persistenceManager->persistAll();

            $this->addFlashMessage("Sucessfully removed the topic!");
            $this->redirect('index', 'forum', null, null);
        } else {
            $this->postRepository->remove($post);
            $this->persistenceManager->persistAll();
            $this->addFlashMessage("Sucessfully removed the post!");
            $this->redirect('index', 'topic', null, array('topic' => $topic));
        }

    }

    /**
     * @param Post $post
     */
    public function editPostAction(Post $post)
    {
        $topic = $post->getTopic();
        $this->postRepository->update($post);
        $this->redirect('index', 'topic', null, array('topic' => $topic));
    }

    /**
     * @param Post $post
     */
    public function showAction(Post $post){
        $currentAccount = $this->securityContext->getAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
        } else {
            $this->redirect('index', 'login', null, null);
        }

        $this->view->assign('post', $post);
    }

}
