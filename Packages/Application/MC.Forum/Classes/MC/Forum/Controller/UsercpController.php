<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;

class UsercpController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('foos', array(
            'bar', 'baz'
        ));
    }
}
