<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Post;
use MC\Forum\Domain\Model\Thank;
use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;

class ThankController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\ThankRepository
     */
    protected $thankRepository;

    /**
     * @return void
     */
    public function indexAction()
    {
    }

    /**
     * @param User $receiver
     * @param Post $post
     */
    public function giveThankAction(User $receiver, Post $post)
    {
        $newThank = new Thank();
        $newThank->setCreator($this->accountService->getLoggedInUser());
        $newThank->setReceiver($receiver);
        $newThank->setPost($post);
        $receiver->addReceivedThank($newThank);
        $this->accountService->getLoggedInUser()->addSentThank($newThank);
        $this->thankRepository->add($newThank);
        $this->persistenceManager->persistAll();
        $this->redirect('index', 'topic', null, array('topic' => $post->getTopic()));
    }

    /**
     * @param Post $post
     */
    public function removeThankAction(Post $post)
    {
        $thanks = $this->thankRepository->findByPost($post);

        foreach ($thanks as $thank) {
            if ($thank->getCreator() == $this->accountService->getLoggedInUser()){
                $this->thankRepository->remove($thank);
                $this->persistenceManager->persistAll();
                $this->redirect('index', 'topic', null, array('topic' => $post->getTopic()));
            }
        }
    }
}