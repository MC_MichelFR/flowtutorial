<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */
use MC\Forum\Domain\Model\User;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Security\Account;

class UserController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @var \MC\Forum\Domain\Repository\MessageRepository
     * @Flow\Inject()
     */
    protected $messageRepository;

    /**
     * @var \MC\Forum\Domain\Repository\TopicRepository
     * @Flow\Inject()
     */
    protected $topicRepository;

    /**
     * @var \MC\Forum\Domain\Repository\PostRepository
     * @Flow\Inject()
     */
    protected $postRepository;

    /**
     * @var \MC\Forum\Domain\Repository\ShoutboxCommentRepository
     * @Flow\Inject()
     */
    protected $shoutboxComments;

    /**
     * @return void
     */
    public function indexAction()
    {
        $currentAccount = $this->securityContext->getAccount();
        $this->view->assign('users', $this->userRepository->findAll());

        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
        } else {
            $this->redirect('index', 'login', null, null);
        }
    }

    /**
     * @param User $user
     */
    public function uploadProfileImageAction(User $user)
    {
        $this->userRepository->update($user);
        $this->redirect("show", null, null, array("account" => $user->getAccount()));
    }

    /**
     * @param Account $account
     */
    public function showAction(Account $account)
    {
        $currentAccount = $this->accountService->getLoggedInAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
            $this->view->assign('currentUser', $this->accountService->getLoggedInUser());
        } else {
            $this->redirect('index', 'login', null, null);
        }

        $user = $this->userRepository->findOneByAccount($account);
        $this->view->assign('user', $user);
    }

    /**
     * @param User $user
     */
    public function removeUserAction(User $user = null)
    {
        if ($user != null) {
            $this->persistenceManager->persistAll();
            foreach ($user->getSentMessages() as $sendMessage) {
                $this->messageRepository->remove($sendMessage);
            }
            foreach ($user->getReceivedMessages() as $receivedMessage) {
                $this->messageRepository->remove($receivedMessage);
            }
            foreach ($user->getPosts() as $post) {
                $this->postRepository->remove($post);
            }
            foreach ($user->getTopics() as $topic) {
                $this->topicRepository->remove($topic);
            }
            foreach ($user->getShoutboxComments() as $shoutboxComment) {
                $this->shoutboxComments->remove($shoutboxComment);
            }
            if($user->getProfileImage() != null) $user->getProfileImage()->shutdownObject();
            $this->userRepository->remove($user);
            $this->accountService->removeAccount($user->getAccount());
            $this->redirect("index", "Forum");
        }
    }

    /**
     * @param User $user
     */
    public function updateAction(User $user)
    {
        $this->userRepository->update($user);
    }
}