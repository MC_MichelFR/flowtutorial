<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use TYPO3\Flow\Persistence\QueryInterface;

abstract class AbstractBaseController extends ActionController
{

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @param \TYPO3\Flow\Mvc\View\ViewInterface $view
     */
    public function initializeView(\TYPO3\Flow\Mvc\View\ViewInterface $view)
    {
        parent::initializeView($view);

        $currentAccount = $this->securityContext->getAccount();
        $view->assign('account', $currentAccount);
    }


}