<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Post;
use MC\Forum\Domain\Model\Subforum;
use MC\Forum\Domain\Model\Topic;
use TYPO3\Flow\Annotations as Flow;

class TopicController extends \MC\Forum\Controller\AbstractBaseController
{
    /**
     * @Flow\Inject
     * @var \TYPO3\Flow\Security\AccountRepository
     */
    protected $accountRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\TopicRepository
     */
    protected $topicRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\PostRepository
     */
    protected $postRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\SubforumRepository
     */
    protected $subforumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @param Topic $topic
     */
    public function indexAction(Topic $topic)
    {
        $currentAccount = $this->securityContext->getAccount();

        //Get User
        $user = $this->userRepository->findOneByAccount($this->securityContext->getAccount());

        $this->view->assign('account', $currentAccount);
        $this->view->assign('user', $user);

        $now = new \DateTime('now');
        $this->view->assign('topic', $topic);
        $this->view->assign('now', $now);
    }

    /**
     * @param Subforum $subforum
     */
    public function newTopicAction(Subforum $subforum)
    {
        $this->view->assign('subforum', $subforum);
    }

    /**
     * @param Post $newPostTopic
     * @param Subforum $subforum
     */
    public function createTopicAction(Post $newPostTopic, Subforum $subforum)
    {
        //Get User
        $user = $this->userRepository->findOneByAccount($this->securityContext->getAccount());

        //Add Topic
        $newTopic = new Topic();
        $newTopic->setUser($user);
        $newTopic->setName($newPostTopic->getTitle());
        $newTopic->setSubforum($subforum);
        $newTopic->setTimestamp(new \DateTime("now"));
        $this->topicRepository->add($newTopic);

        //Add Post
        $newPostTopic->setUser($user);
        $newPostTopic->setTimestamp(new \DateTime("now"));
        $newPostTopic->setTopic($newTopic);
        $newPostTopic->setTimestamp($newTopic->getTimestamp());
        $newPostTopic->setMainPost(true);
        $this->postRepository->add($newPostTopic);

        $this->redirect('index', 'topic', null, array('topic' => $newTopic));
    }

    /**
     * @param Topic $topic
     * @param Post $newPost
     */
    public function createPostAction(Topic $topic, Post $newPost)
    {
        $user = $this->userRepository->findOneByAccount($this->securityContext->getAccount());
        $newPost->setUser($user);
        $newPost->setTopic($topic);
        $newPost->setTimestamp(new \DateTime("now"));
        $newPost->setMainPost(false);
        $this->postRepository->add($newPost);

        $this->redirect('index', 'topic', null, array('topic' => $topic));
    }

    /**
     * @param Topic $topic
     */
    public function delTopicAction(Topic $topic)
    {
        foreach ($topic->getPosts() as $post) {
            $this->postRepository->remove($post);
            $this->persistenceManager->persistAll();
        }

        $subforum = $topic->getSubforum();
        $this->topicRepository->remove($topic);
        $this->persistenceManager->persistAll();

        $this->addFlashMessage("Successfully removed the topic!");
        $this->redirect('index', 'subforum', null, array('subforum' => $subforum));
    }

    /**
     * @param Topic $topic
     */
    public function editTopicAction(Topic $topic)
    {
        $this->topicRepository->update($topic);
        $this->redirect('index', 'topic', null, array('topic' => $topic));
    }


}