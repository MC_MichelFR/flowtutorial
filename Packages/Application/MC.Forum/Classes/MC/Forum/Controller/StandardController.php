<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Forum;
use MC\Forum\Domain\Model\Subforum;
use MC\Forum\Domain\Model\Topic;
use TYPO3\Flow\Annotations as Flow;

class StandardController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\ForumRepository
     */
    protected $forumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\SubforumRepository
     */
    protected $subforumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\TopicRepository
     */
    protected $topicRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\PostRepository
     */
    protected $postRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\UserRepository
     */
    protected $userRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\MessageRepository
     */
    protected $messageRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @return void
     */
    public function indexAction()
    {
        $currentAccount = $this->securityContext->getAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
        } else {
            $this->redirect('index', 'login', null, null);
        }

        $this->redirect('index', 'forum', null, null);
    }

}
