<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Subforum;
use MC\Forum\Domain\Model\Topic;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

class SubforumController extends \MC\Forum\Controller\AbstractBaseController
{

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\SubforumRepository
     */
    protected $subforumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\TopicRepository
     */
    protected $topicRepository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @param Subforum $subforum
     */
    public function indexAction(Subforum $subforum)
    {
        $currentAccount = $this->securityContext->getAccount();
        if ($currentAccount != "") {
            $this->view->assign('account', $currentAccount);
        } else {
            $this->redirect('index', 'login', null, null);
        }

        $this->view->assign('subforum', $subforum);
    }

}
