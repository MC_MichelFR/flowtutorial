<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\Forum;
use MC\Forum\Domain\Model\Subforum;
use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\QueryInterface;

class ForumController extends \MC\Forum\Controller\AbstractBaseController
{
    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\ForumRepository
     */
    protected $forumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\TopicRepository
     */
    protected $topicRepository;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\PostRepository
     */
    protected $postRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\SubforumRepository
     */
    protected $subforumRepository;

    /**
     * @Flow\Inject
     * @var \MC\Forum\Domain\Repository\UserRepository
     */
    protected $userRespository;

    /**
     * @var \TYPO3\Flow\Security\Context
     * @Flow\Inject
     */
    protected $securityContext;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->forumRepository->setDefaultOrderings(array("position" => QueryInterface::ORDER_ASCENDING));
        $this->userRespository->setDefaultOrderings(array("creationDate" => QueryInterface::ORDER_DESCENDING));
        $this->view->assign('lastUser', $this->userRespository->findAll()->getFirst());
        $this->view->assign('forums', $this->forumRepository->findAll());
        $this->view->assign('topics', $this->topicRepository->findAll());
        $this->view->assign('users', $this->userRespository->findAll());
        $this->view->assign('posts', $this->postRepository->findAll());
        $this->view->assign('user', $this->accountService->getLoggedInUser());

    }

    /**
     * TODO: make this dynamic so that admin can add new Forums and Subofrums via the Admin Panel
     * This function fills example Forums and Subforums
     *
     * @return void
     */
    public function fillContentAction()
    {
        $newForum1 = new Forum();
        $newForum2 = new Forum();
        $newForum3 = new Forum();
        $newSubforum11 = new Subforum();
        $newSubforum12 = new Subforum();
        $newSubforum13 = new Subforum();
        $newSubforum21 = new Subforum();
        $newSubforum22 = new Subforum();
        $newSubforum31 = new Subforum();
        $newSubforum32 = new Subforum();

        $newForum1->setName("Nachrichten");
        $newForum1->setPosition(1);
            $newSubforum11->setPosition(1);
            $newSubforum11->setName("Politik");
            $newSubforum11->setForum($newForum1);
            $newSubforum12->setPosition(2);
            $newSubforum12->setName("Technik");
            $newSubforum12->setForum($newForum1);
            $newSubforum13->setPosition(3);
            $newSubforum13->setName("International");
            $newSubforum13->setForum($newForum1);

        $newForum2->setName("Musik");
        $newForum2->setPosition(2);
            $newSubforum21->setForum($newForum2);
            $newSubforum21->setName("Charts");
            $newSubforum21->setPosition(1);
            $newSubforum22->setName("Rap");
            $newSubforum22->setForum($newForum2);
            $newSubforum22->setPosition(2);

        $newForum3->setName("Filme");
        $newForum3->setPosition(3);
            $newSubforum31->setName("Aktuelle Kinofilme");
            $newSubforum31->setPosition(1);
            $newSubforum31->setForum($newForum3);
            $newSubforum32->setName("Action");
            $newSubforum32->setPosition(2);
            $newSubforum32->setForum($newForum3);

        $this->forumRepository->add($newForum1);
        $this->forumRepository->add($newForum2);
        $this->forumRepository->add($newForum3);
        $this->subforumRepository->add($newSubforum11);
        $this->subforumRepository->add($newSubforum12);
        $this->subforumRepository->add($newSubforum13);
        $this->subforumRepository->add($newSubforum21);
        $this->subforumRepository->add($newSubforum22);
        $this->subforumRepository->add($newSubforum31);
        $this->subforumRepository->add($newSubforum32);
        $this->persistenceManager->persistAll();

        $this->redirect("index");
    }
}