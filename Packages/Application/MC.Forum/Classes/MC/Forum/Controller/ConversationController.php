<?php
namespace MC\Forum\Controller;

/*
 * This file is part of the MC.Forum package.
 */

use MC\Forum\Domain\Model\User;
use MC\Forum\Service\ConversationService;
use TYPO3\Flow\Annotations as Flow;

class ConversationController extends \TYPO3\Flow\Mvc\Controller\ActionController
{

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Domain\Repository\ConversationRepository
     */
    protected $conversationRepository;

    /**
     * @Flow\Inject()
     * @var \MC\Forum\Service\AccountService
     */
    protected $accountService;

    /**
     * @var ConversationService
     * @Flow\Inject
     */
    protected $conversationService;

    /**
     * @param User $conversationPartner
     */
    public function indexAction(User $conversationPartner = null)
    {
        $conversations = $this->conversationService->getConversations($this->accountService->getLoggedInUser());
        if ($conversationPartner != null) {
            $this->view->assign("messages", $this->conversationService->getConversation($conversationPartner));
            $this->view->assign('conversationPartner', $conversationPartner);
        } else {

        }

        $this->view->assign('account', $this->accountService->getLoggedInAccount());
        $this->view->assign('user', $this->accountService->getLoggedInUser());
        $this->view->assign('conversations', $conversations);
    }

}
